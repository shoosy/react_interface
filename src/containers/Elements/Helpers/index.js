import React from 'react';
import {Row} from 'react-bootstrap';
import {Widget, WidgetContent, WidgetPanel, WidgetTitle} from '../../../components/common/Widget';
import Table from '../../../components/common/Table';
import Page from '../../../components/Page'
import {text, background as b,
        padding as p, margin as m,
        borderRadius as r,
        image as img,
        font as f,
        border as brd} from '../../../components/common/Helpers'

class Helpers extends React.Component {

    render() {
        return (
            <Page>
                <Row>
                    <div className="col-lg-6">
                        <WidgetPanel>
                            <WidgetTitle title="Contextual colors"/>
                            <WidgetContent>
                                <Table>
                                    <thead>
                                        <tr>
                                            <th>
                                                Class name
                                            </th>
                                            <th>
                                                Example
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <code>.muted</code>
                                            </td>
                                            <td>
                                                <span className={text.muted}>Example text</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.primary</code>
                                            </td>
                                            <td>
                                                <span className={text.primary}>Example text</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.success</code>
                                            </td>
                                            <td>
                                                <span className={text.success}>Example text</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.info</code>
                                            </td>
                                            <td>
                                                <span className={text.info}>Example text</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.warning</code>
                                            </td>
                                            <td>
                                                <span className={text.warning}>Example text</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.danger</code>
                                            </td>
                                            <td>
                                                <span className={text.danger}>Example text</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </WidgetContent>
                        </WidgetPanel>
                        <WidgetPanel>
                            <WidgetTitle title="Border radius"/>
                            <WidgetContent>
                                <Table>
                                    <thead>
                                        <tr>
                                            <th>
                                                Class name
                                            </th>
                                            <th>
                                                Example
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <code>.xs</code>
                                            </td>
                                            <td>
                                                <div className={`${b.primary} ${p.xs} ${r.xs}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.sm</code>
                                            </td>
                                            <td>
                                                <div className={`${b.primary} ${p.xs} ${r.sm}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.md</code>
                                            </td>
                                            <td>
                                                <div className={`${b.primary} ${p.xs} ${r.md}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.lg</code>
                                            </td>
                                            <td>
                                                <div className={`${b.primary} ${p.xs} ${r.lg}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.xl</code>
                                            </td>
                                            <td>
                                                <div className={`${b.primary} ${p.xs} ${r.xl}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </WidgetContent>
                        </WidgetPanel>
                        <WidgetPanel>
                            <WidgetTitle title="Hr Line"/>
                            <WidgetContent>
                                <Table>
                                    <thead>
                                        <tr>
                                            <th>
                                                Class name
                                            </th>
                                            <th>
                                                Example
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Standard
                                                <code>&lt;hr&gt;</code>
                                            </td>
                                            <td><hr/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.hrLineDashed</code>
                                            </td>
                                            <td><hr className="hrLineDashed"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.hrLineSolid</code>
                                            </td>
                                            <td><hr className="hrLineSolid"/></td>
                                        </tr>

                                    </tbody>
                                </Table>
                            </WidgetContent>
                        </WidgetPanel>
                        <WidgetPanel>
                            <WidgetTitle title="Paddings"/>
                            <WidgetContent>
                                <Table>
                                    <thead>
                                        <tr>
                                            <th>
                                                Class name
                                            </th>
                                            <th>
                                                Example
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <code>.xxs</code>
                                            </td>
                                            <td>
                                                <div className={`${b.primary} ${p.xxs}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.pXs</code>
                                            </td>
                                            <td>
                                                <div className={`${b.primary} ${p.xs}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.pSm</code>
                                            </td>
                                            <td>
                                                <div className={`${b.primary} ${p.sm}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <code>.pMd</code>
                                            </td>
                                            <td>
                                                <div className={`${b.primary} ${p.md}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.pLg</code>
                                            </td>
                                            <td>
                                                <div className={`${b.primary} ${p.lg}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.pXl</code>
                                            </td>
                                            <td>
                                                <div className={`${b.primary} ${p.xl}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.noPaddings</code><br/>
                                                <small className="textMuted">remove all paddings</small>
                                            </td>
                                            <td>
                                                <div className={`${b.primary} ${p.none}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.pW-(xs,sm,md,lg,xl)</code><br/>
                                                <small>padding width</small>
                                            </td>
                                            <td>
                                                <div className={`${b.primary} ${p.mdWidth}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.pH-(xs,sm,md,lg,xl)</code><br/>
                                                <small>height width</small>
                                            </td>
                                            <td>
                                                <div className={`${b.primary} ${p.mdHeight}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>

                                    </tbody>
                                </Table>
                            </WidgetContent>
                        </WidgetPanel>
                        <WidgetPanel>
                            <WidgetTitle title="Borders"/>
                            <WidgetContent>
                                <Table>
                                    <thead>
                                        <tr>
                                            <th>
                                                Class name
                                            </th>
                                            <th>
                                                Example
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <code>.left</code>
                                            </td>
                                            <td>
                                                <div className={`${p.xs} ${brd.left}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.right</code>
                                            </td>
                                            <td>
                                                <div className={`${p.xs} ${brd.right}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.top</code>
                                            </td>
                                            <td>
                                                <div className={`${p.xs} ${brd.top}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.bottom</code>
                                            </td>
                                            <td>
                                                <div className={`${p.xs} ${brd.bottom}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.none</code><br/>
                                                <small className={text.muted}>remove all borders</small>
                                            </td>
                                            <td>
                                                <div className={`${p.xs} ${brd.none}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.leftRight</code>
                                            </td>
                                            <td>
                                                <div className={`${p.xs} ${brd.leftRight}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.topBottom</code>
                                            </td>
                                            <td>
                                                <div className={`${p.xs} ${brd.topBottom}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.size(sm,md,lg,xl)</code>
                                            </td>
                                            <td>
                                                <div className={`${p.xs} ${brd.topBottom} ${brd.leftRight} ${brd.sizelg}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>

                                    </tbody>
                                </Table>
                            </WidgetContent>
                        </WidgetPanel>
                    </div>
                    <div className="col-lg-6">                    
                        <WidgetPanel>
                            <WidgetTitle title="Contextual backgrounds"/>
                            <WidgetContent>
                                <Table>
                                    <thead>
                                        <tr>
                                            <th>
                                                Class name
                                            </th>
                                            <th>
                                                Example
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <code>.muted</code>
                                            </td>
                                            <td>
                                                <div className={`${b.muted} ${p.xs} ${r.sm}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.primary</code>
                                            </td>
                                            <td>
                                                <div className={`${b.primary} ${p.xs} ${r.sm}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.success</code>
                                            </td>
                                            <td>
                                                <div className={`${b.success} ${p.xs} ${r.sm}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.info</code>
                                            </td>
                                            <td>
                                                <div className={`${b.info} ${p.xs} ${r.sm}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.warning</code>
                                            </td>
                                            <td>
                                                <div className={`${b.warning} ${p.xs} ${r.sm}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.danger</code>
                                            </td>
                                            <td>
                                                <div className={`${b.danger} ${p.xs} ${r.sm}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </WidgetContent>
                        </WidgetPanel>
                        <WidgetPanel>
                            <WidgetTitle title="Images"/>
                            <WidgetContent>
                                <Table>
                                    <thead>
                                        <tr>
                                            <th>
                                                Class name
                                            </th>
                                            <th>
                                                Example
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <code>.circle</code>
                                            </td>
                                            <td><img src="/src/components/img/a6.jpg" className={`${img.md} ${img.circle}`}/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.rounded</code>
                                            </td>
                                            <td><img src="/src/components/img/a6.jpg" className={`${img.md} ${img.rounded}`}/></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.thumbnail</code>
                                            </td>
                                            <td><img src="/src/components/img/a6.jpg" className={`${img.md} ${img.thumbnail}`}/></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.sm</code>
                                            </td>
                                            <td><img src="/src/components/img/a6.jpg" className={`${img.sm}`}/></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.md</code>
                                            </td>
                                            <td><img src="/src/components/img/a6.jpg" className={`${img.md}`}/></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.lg</code>
                                            </td>
                                            <td><img src="/src/components/img/a6.jpg" className={`${img.lg}`}/></td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </WidgetContent>
                        </WidgetPanel>
                        <WidgetPanel>
                            <WidgetTitle title="Basic font width"/>
                            <WidgetContent>
                                <Table>
                                    <thead>
                                        <tr>
                                            <th>
                                                Class name
                                            </th>
                                            <th>
                                                Description
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <code>.normal</code>
                                            </td>
                                            <td>
                                                <span className={`${f.normal}`}>Example text</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.bold</code>
                                            </td>
                                            <td>
                                                <span className={`${f.bold}`}>Example text</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.italic</code>
                                            </td>
                                            <td>
                                                <span className={`${f.italic}`}>Example text</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.uppercase</code>
                                            </td>
                                            <td>
                                                <span className={`${text.uppercase}`}>Example text</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.lowercase</code>
                                            </td>
                                            <td>
                                                <span className={`${text.lowercase}`}>Example text</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.capitalize</code>
                                            </td>
                                            <td>
                                                <span className={`${text.capitalize}`}>Example text</span>
                                            </td>
                                        </tr>

                                    </tbody>
                                </Table>
                            </WidgetContent>
                        </WidgetPanel>
                        <WidgetPanel>
                            <WidgetTitle title="Margins"/>
                            <WidgetContent>
                                <Table>
                                    <thead>
                                        <tr>
                                            <th>
                                                Class name
                                            </th>
                                            <th>
                                                Example
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <code>.mXxs</code>
                                            </td>
                                            <td>
                                                <div className={`${m.xxs} ${b.muted} ${p.xs}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.mXs</code>
                                            </td>
                                            <td>
                                                <div className={`${m.xs} ${b.muted} ${p.xs}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.mSm</code>
                                            </td>
                                            <td>
                                                <div className={`${m.sm} ${b.muted} ${p.xs}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <code>.mMd</code>
                                            </td>
                                            <td>
                                                <div className={`${m.md} ${b.muted} ${p.xs}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.mLg</code>
                                            </td>
                                            <td>
                                                <div className={`${m.lg} ${b.muted} ${p.xs}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.mXl</code>
                                            </td>
                                            <td>
                                                <div className={`${m.xl} ${b.muted} ${p.xs}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.mN</code>
                                            </td>
                                            <td>
                                                <div className={`${m.none} ${b.muted} ${p.xs}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.mL / mL-(xs,sm,md,lg,xl)</code><br/>
                                                <small>margin left</small>
                                            </td>
                                            <td>
                                                <div className={`${m.leftxs} ${b.muted} ${p.xs}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.mLN / mLN-(xs,sm,md,lg,xl)</code><br/>
                                                <small>margin left negative</small>
                                            </td>
                                            <td>
                                                <div className={`${m.leftN} ${b.muted} ${p.xs}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.mLNone</code><br/>
                                                <small>clear margin left</small>
                                            </td>
                                            <td>
                                                <div className={`${m.leftNone} ${b.muted} ${p.xs}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.mT / mT-(xs,sm,md,lg,xl)</code><br/>
                                                <small>margin top</small>
                                            </td>
                                            <td>
                                                <div className={`${m.top} ${b.muted} ${p.xs}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.mTN / mTN-(xs,sm,md,lg,xl)</code><br/>
                                                <small>margin top negative</small>
                                            </td>
                                            <td>
                                                <div className={`${m.topN} ${b.muted} ${p.xs}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.mTNone</code><br/>
                                                <small>clear margin top</small>
                                            </td>
                                            <td>
                                                <div className={`${m.topNone} ${b.muted} ${p.xs}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.mR / mR-(xs,sm,md,lg,xl)</code><br/>
                                                <small>margin right</small>
                                            </td>
                                            <td>
                                                <div className={`${m.right} ${b.muted} ${p.xs}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.mRN / mRN-(xs,sm,md,lg,xl)</code><br/>
                                                <small>margin right negative</small>
                                            </td>
                                            <td>
                                                <div className={`${m.rightN} ${b.muted} ${p.xs}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.mRNone</code><br/>
                                                <small>clear margin right</small>
                                            </td>
                                            <td>
                                                <div className={`${m.rightNone} ${b.muted} ${p.xs}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.mB / mB-(xs,sm,md,lg,xl)</code><br/>
                                                <small>margin bottom</small>
                                            </td>
                                            <td>
                                                <div className={`${m.bottom} ${b.muted} ${p.xs}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.mBN / mBN-(xs,sm,md,lg,xl)</code><br/>
                                                <small>margin bottom negative</small>
                                            </td>
                                            <td>
                                                <div className={`${m.bottomN} ${b.muted} ${p.xs}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.mBNone</code><br/>
                                                <small>clear margin bottom</small>
                                            </td>
                                            <td>
                                                <div className={`${m.bottomNone} ${b.muted} ${p.xs}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <code>.noMargins</code><br/>
                                                <small>remove all margins</small>
                                            </td>
                                            <td>
                                                <div className={`${m.none} ${b.muted} ${p.xs}`}>
                                                    Example text</div>
                                            </td>
                                        </tr>

                                    </tbody>
                                </Table>
                            </WidgetContent>
                        </WidgetPanel>
                    </div>
                </Row>
            </Page>
        );
    }
}

export default Helpers;
