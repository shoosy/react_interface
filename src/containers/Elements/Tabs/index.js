import React from 'react';
import ReactDOM from 'react-dom'
import {Row} from 'react-bootstrap';
import {TabContainer, TabContent, TabPanel, TabNav, TabNavItem} from '../../../components/common/Tabs';
import Page from '../../../components/Page'
import Icon from '../../../components/Icon'
import Tag from '../../../components/common/Tag'
import {margin as m} from '../../../components/common/Helpers'

class Tabs extends React.Component {

    render() {
        return (
            <Page>
                <Row className={m.toplg}>
                    <TabContainer>
                            <TabNav>
                                <TabNavItem active tableKey="tab-1">This is tab</TabNavItem>
                                <TabNavItem tableKey="tab-2">This is second tab</TabNavItem>
                            </TabNav>
                            <TabContent>
                                <TabPanel tableKey="tab-1" active>
                                    <strong>Lorem ipsum dolor sit amet, consectetuer adipiscing</strong>

                                    <p>A wonderful serenity has taken possession of my entire soul, like these sweet
                                        mornings of spring which I enjoy with my whole heart. I am alone, and feel the
                                        charm of existence in this spot, which was created for the bliss of souls like
                                        mine.</p>

                                    <p>I am so happy, my dear friend, so absorbed in the exquisite sense of mere
                                        tranquil existence, that I neglect my talents. I should be incapable of drawing
                                        a single stroke at the present moment; and yet I feel that I never was a greater
                                        artist than now. When.</p>
                                </TabPanel>
                                <TabPanel tableKey="tab-2">
                                        <strong>Donec quam felis</strong>

                                        <p>Thousand unknown plants are noticed by me: when I hear the buzz of the little
                                            world among the stalks, and grow familiar with the countless indescribable forms
                                            of the insects and flies, then I feel the presence of the Almighty, who formed
                                            us in his own image, and the breath
                                        </p>

                                        <p>I am alone, and feel the charm of existence in this spot, which was created
                                            for the bliss of souls like mine. I am so happy, my dear friend, so absorbed in
                                            the exquisite sense of mere tranquil existence, that I neglect my talents. I
                                            should be incapable of drawing a single stroke at the present moment; and yet.</p>
                                </TabPanel>
                            </TabContent>
                    </TabContainer>
                    <TabContainer>
                            <TabNav>
                                <TabNavItem active tableKey="tab-1"><Icon name="laptop"/></TabNavItem>
                                <TabNavItem tableKey="tab-2"><Icon name="desktop"/></TabNavItem>
                                <TabNavItem tableKey="tab-3"><Icon name="database"/></TabNavItem>
                            </TabNav>
                            <TabContent>
                                <TabPanel tableKey="tab-1" active>
                                    <strong>Lorem ipsum dolor sit amet, consectetuer adipiscing</strong>

                                    <p>A wonderful serenity has taken possession of my entire soul, like these sweet
                                        mornings of spring which I enjoy with my whole heart. I am alone, and feel the
                                        charm of existence in this spot, which was created for the bliss of souls like
                                        mine.</p>

                                    <p>I am so happy, my dear friend, so absorbed in the exquisite sense of mere
                                        tranquil existence, that I neglect my talents. I should be incapable of drawing
                                        a single stroke at the present moment; and yet I feel that I never was a greater
                                        artist than now. When.</p>
                                </TabPanel>
                                <TabPanel tableKey="tab-2">
                                        <strong>Donec quam felis</strong>

                                        <p>Thousand unknown plants are noticed by me: when I hear the buzz of the little
                                            world among the stalks, and grow familiar with the countless indescribable forms
                                            of the insects and flies, then I feel the presence of the Almighty, who formed
                                            us in his own image, and the breath
                                        </p>

                                        <p>I am alone, and feel the charm of existence in this spot, which was created
                                            for the bliss of souls like mine. I am so happy, my dear friend, so absorbed in
                                            the exquisite sense of mere tranquil existence, that I neglect my talents. I
                                            should be incapable of drawing a single stroke at the present moment; and yet.</p>
                                </TabPanel>
                                <TabPanel tableKey="tab-3">
                                        <strong>Donec quam felis</strong>

                                        <p>Thousand unknown plants are noticed by me: when I hear the buzz of the little
                                            world among the stalks, and grow familiar with the countless indescribable forms
                                            of the insects and flies, then I feel the presence of the Almighty, who formed
                                            us in his own image, and the breath
                                        </p>

                                        <p>I am alone, and feel the charm of existence in this spot, which was created
                                            for the bliss of souls like mine. I am so happy, my dear friend, so absorbed in
                                            the exquisite sense of mere tranquil existence, that I neglect my talents. I
                                            should be incapable of drawing a single stroke at the present moment; and yet.</p>
                                </TabPanel>
                            </TabContent>
                    </TabContainer>
                </Row>

                <Row className={m.toplg}>
                    <TabContainer left>
                            <TabNav>
                                <TabNavItem active tableKey="tab-1">This is tab</TabNavItem>
                                <TabNavItem tableKey="tab-2">This is second tab</TabNavItem>
                            </TabNav>
                            <TabContent>
                                <TabPanel tableKey="tab-1" active>
                                    <strong>Lorem ipsum dolor sit amet, consectetuer adipiscing</strong>

                                    <p>A wonderful serenity has taken possession of my entire soul, like these sweet
                                        mornings of spring which I enjoy with my whole heart. I am alone, and feel the
                                        charm of existence in this spot, which was created for the bliss of souls like
                                        mine.</p>

                                    <p>I am so happy, my dear friend, so absorbed in the exquisite sense of mere
                                        tranquil existence, that I neglect my talents. I should be incapable of drawing
                                        a single stroke at the present moment; and yet I feel that I never was a greater
                                        artist than now. When.</p>
                                </TabPanel>
                                <TabPanel tableKey="tab-2">
                                        <strong>Donec quam felis</strong>

                                        <p>Thousand unknown plants are noticed by me: when I hear the buzz of the little
                                            world among the stalks, and grow familiar with the countless indescribable forms
                                            of the insects and flies, then I feel the presence of the Almighty, who formed
                                            us in his own image, and the breath
                                        </p>

                                        <p>I am alone, and feel the charm of existence in this spot, which was created
                                            for the bliss of souls like mine. I am so happy, my dear friend, so absorbed in
                                            the exquisite sense of mere tranquil existence, that I neglect my talents. I
                                            should be incapable of drawing a single stroke at the present moment; and yet.</p>
                                </TabPanel>
                            </TabContent>
                    </TabContainer>
                    <TabContainer right>
                            <TabNav>
                                <TabNavItem active tableKey="tab-1">This is tab</TabNavItem>
                                <TabNavItem tableKey="tab-2">This is second tab</TabNavItem>
                            </TabNav>
                            <TabContent>
                                <TabPanel key="tab-1" active>
                                    <strong>Lorem ipsum dolor sit amet, consectetuer adipiscing</strong>

                                    <p>A wonderful serenity has taken possession of my entire soul, like these sweet
                                        mornings of spring which I enjoy with my whole heart. I am alone, and feel the
                                        charm of existence in this spot, which was created for the bliss of souls like
                                        mine.</p>

                                    <p>I am so happy, my dear friend, so absorbed in the exquisite sense of mere
                                        tranquil existence, that I neglect my talents. I should be incapable of drawing
                                        a single stroke at the present moment; and yet I feel that I never was a greater
                                        artist than now. When.</p>
                                </TabPanel>
                                <TabPanel key="tab-2">
                                        <strong>Donec quam felis</strong>

                                        <p>Thousand unknown plants are noticed by me: when I hear the buzz of the little
                                            world among the stalks, and grow familiar with the countless indescribable forms
                                            of the insects and flies, then I feel the presence of the Almighty, who formed
                                            us in his own image, and the breath
                                        </p>

                                        <p>I am alone, and feel the charm of existence in this spot, which was created
                                            for the bliss of souls like mine. I am so happy, my dear friend, so absorbed in
                                            the exquisite sense of mere tranquil existence, that I neglect my talents. I
                                            should be incapable of drawing a single stroke at the present moment; and yet.</p>
                                </TabPanel>
                            </TabContent>
                    </TabContainer>
                </Row>

                <Row className={m.toplg}>
                    <TabContainer lg="12">
                            <TabNav>
                                <TabNavItem active tableKey="tab-1">This is tab <Tag label right type="warning" title="NEW"/></TabNavItem>
                            </TabNav>
                            <TabContent>
                                <TabPanel tableKey="tab-1" active>
                                    <strong>Lorem ipsum dolor sit amet, consectetuer adipiscing</strong>

                                    <p>A wonderful serenity has taken possession of my entire soul, like these sweet
                                        mornings of spring which I enjoy with my whole heart. I am alone, and feel the
                                        charm of existence in this spot, which was created for the bliss of souls like
                                        mine.</p>

                                    <p>I am so happy, my dear friend, so absorbed in the exquisite sense of mere
                                        tranquil existence, that I neglect my talents. I should be incapable of drawing
                                        a single stroke at the present moment; and yet I feel that I never was a greater
                                        artist than now. When.</p>
                                </TabPanel>
                            </TabContent>
                    </TabContainer>
                </Row>
            </Page>
        );
    }
}

export default Tabs;
