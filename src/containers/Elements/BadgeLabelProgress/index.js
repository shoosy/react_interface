import React from 'react';
import {Row} from 'react-bootstrap';
import {Widget, WidgetContent} from '../../../components/common/Widget';
import Tag from '../../../components/common/Tag';
import Page from '../../../components/Page'
import {Progress, Bar} from '../../../components/common/Progress';

class BadgeLabelProgress extends React.Component {

    render() {
        var width = {width: "35%"};
        return (
                <Page>
                    <Row>                  
                        <Widget column="6" size="lg" title="Badges" tools>
                            <WidgetContent>
                                <p>
                                    To add a badge style You have to add <code>.badge</code>class to element. To change a color od badge you can add extra class like <code>.badge-primary</code>.
                                </p>
                                <p><Tag badge title="Plain"/></p>
                                <p><Tag badge type="primary" title="Primary"/></p>
                                <p><Tag badge type="info" title="Information"/></p>
                                <p><Tag badge type="success" title="Success"/></p>
                                <p><Tag badge type="warning" title="Warning"/></p>
                                <p><Tag badge type="danger" title="Danger"/></p>
                            </WidgetContent>
                        </Widget>
                        <Widget column="6" size="lg" title="labels" tools>
                            <WidgetContent>
                                <p>
                                    Analogic to badge. To add a label style You have to add <code>.label</code>class to element. To change a color od label you can add extra class like <code>.label-primary</code>.
                                </p>
                                <p><Tag label title="Plain"/></p>
                                <p><Tag label type="primary" title="Primary"/></p>
                                <p><Tag label type="info" title="Information"/></p>
                                <p><Tag label type="success" title="Success"/></p>
                                <p><Tag label type="warning" title="Warning"/></p>
                                <p><Tag label type="danger" title="Danger"/></p>
                            </WidgetContent>
                        </Widget>
                    </Row>
                    <Row>
                        <Widget col={12} size="lg" title="Progress Bars" tools>
                            <WidgetContent> 
                                <h5>Basic ProgressBars</h5>
                                <Progress>
                                    <Bar max="100" min="0" value="35" type="success" title="35% Complete (success)"/>
                                </Progress>                                
                                <Progress>
                                    <Bar max="100" min="0" value="35" type="success"  title="35% Complete (success)" right/>
                                </Progress>                                
                                <Progress>
                                    <Bar max="100" min="0" value="43" title="43% Complete (success)"/>
                                </Progress>

                                <h5>Striped Progressbars</h5>

                                <Progress striped>
                                    <Bar max="100" min="0" value="50" type="warning" title="40% Complete (success)"/>
                                </Progress>

                                <h5>Active Progressbars</h5>

                                <Progress striped active>
                                    <Bar max="100" min="0" value="75" type="danger" title="75% Complete (success)"/>
                                </Progress>

                                <h5>Stacked Progressbars</h5>

                                <Progress striped active>
                                    <Bar value={30} type="success" title="30% Complete (success)"/>
                                    <Bar value={20} type="warning" title="20% Complete (success)"/>
                                    <Bar value={40} type="danger" title="40% Complete (success)"/>
                                </Progress>
                            </WidgetContent>
                        </Widget>
                    </Row>
                </Page>
            );
    }
}

export default BadgeLabelProgress;

