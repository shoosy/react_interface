import React from 'react';
import ReactDOM from 'react-dom'
import {Row} from 'react-bootstrap';
import {Widget, WidgetContent, WidgetPanel, WidgetTitle} from '../../../components/common/Widget';
import { Alert } from '../../../components/common/Alert';
import TooltipButton from '../../../components/common/TooltipButton';
import PopoverButton from '../../../components/common/PopoverButton';
import Page from '../../../components/Page'

class Notifications extends React.Component {

    render() {
        return (
            <Page>
                <Row>
                    <Widget column="6" size="lg" title="Alerts Styles">
                        <WidgetContent>
                                <Alert>
                                    How quickly daft jumping zebras vex. <a className="alert-link" href="#">Alert Link</a>.
                                </Alert>
                                <Alert type="info">
                                    How quickly daft jumping zebras vex. <a className="alert-link" href="#">Alert Link</a>.
                                </Alert>
                                <Alert type="warning">
                                    How quickly daft jumping zebras vex. <a className="alert-link" href="#">Alert Link</a>.
                                </Alert>
                                <Alert type="danger">
                                    How quickly daft jumping zebras vex. <a className="alert-link" href="#">Alert Link</a>.
                                </Alert>
                        </WidgetContent>
                    </Widget>
                    <Widget column="6" size="lg" title="Dismissable Alerts">
                        <WidgetContent>                         
                                <Alert dismissable>
                                    A wonderful serenity has taken possession. <a className="alert-link" href="#">Alert Link</a>.
                                </Alert>
                                <Alert type="info" dismissable>
                                    How quickly daft jumping zebras vex. <a className="alert-link" href="#">Alert Link</a>.
                                </Alert>
                                <Alert type="warning" dismissable>
                                    How quickly daft jumping zebras vex. <a className="alert-link" href="#">Alert Link</a>.
                                </Alert>
                                <Alert id="alert-4" type="danger" dismissable>
                                    How quickly daft jumping zebras vex. <a className="alert-link" href="#">Alert Link</a>.
                                </Alert>   
                        </WidgetContent> 
                    </Widget>
                </Row>
                <Row>
                    <Widget column="12" size="lg" title="Tooltips and Popovers">
                        <WidgetContent textCenter>                                
                            <h4>Tooltip Demo <small>Background color more gentle.</small></h4>
                            <div className="tooltip-demo">
                                <TooltipButton type="default" place="left" title="Tooltip on left" tooltipText="Tooltip on left"/>
                                <TooltipButton type="default" place="top" title="Tooltip on top" tooltipText="Tooltip on top"/>
                                <TooltipButton type="default" place="bottom" title="Tooltip on bottom" tooltipText="Tooltip on bottom"/>
                                <TooltipButton type="default" place="right" title="Tooltip on right" tooltipText="Tooltip on right"/>
                            </div>
                            <br/>
                            <h4>Clickable Popover Demo</h4>
                            <div className="tooltip-demo text-center" >
                                <PopoverButton type="default" place="left" title="Tooltip on left" content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus."/>
                                <PopoverButton type="default" place="top" title="Tooltip on top" content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus."/>
                                <PopoverButton type="default" place="bottom" title="Tooltip on bottom" content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus."/>
                                <PopoverButton type="default" place="right" title="Tooltip on right" content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus."/>
                            </div>
                        </WidgetContent>
                    </Widget>
                </Row>
            </Page>
        );
    }
}

export default Notifications;
