import React from 'react';
import { Collapse, Row, ButtonGroup} from 'react-bootstrap';
import {Widget, WidgetContent} from '../../../components/common/Widget';
import Button from '../../../components/common/Button';
import DropdownButton from '../../../components/common/DropdownButton';
import DropdownItem from '../../../components/common/DropdownItem';
import Page from '../../../components/Page'

class Buttons extends React.Component {

    render() {
        return (
                <Page>                      
                <Widget column={4} size="lg" title="Colors buttons" tools>
                    <WidgetContent>
                        <p>
                            Use any of the available button classes to quickly create a styled button.
                        </p>
                        <h3 className="font-bold">
                            Normal buttons
                        </h3>
                        <p>
                            <Button floatMargin minSize type="default" title="Default"/>
                            <Button floatMargin minSize type="primary" title="Primary"/>
                            <Button floatMargin minSize type="success" title="Success"/>
                            <Button floatMargin minSize type="info" title="Info"/>
                            <Button floatMargin minSize type="warning" title="Warning"/>
                            <Button floatMargin minSize type="danger" title="Danger"/>
                            <Button floatMargin minSize type="link" title="Link"/>
                        </p>
                    </WidgetContent>
                </Widget>
                <Widget column={4} size="lg" title={"Diferent size"} tools>
                    <WidgetContent>                        
                        <p>
                    If You want larger or smaller buttons You can add <code>.btn-lg</code>, <code>.btn-sm</code>, or <code>.btn-xs</code> class.
                        </p>
                        <h3 className="font-bold">Button Sizes</h3>
                        <p>
                            <Button floatMargin type="primary" size="lg" title="Default"/>
                            <Button floatMargin type="default" size="lg" title="Default"/>
                            <br/>
                            <Button floatMargin type="primary" title="Default"/>
                            <Button floatMargin type="default" title="Default"/>
                            <br/>
                            <Button floatMargin type="primary" size="sm" title="Default"/>
                            <Button floatMargin type="default" size="sm" title="Default"/>
                            <br/>
                            <Button floatMargin type="primary" size="xs" title="Default"/>
                            <Button floatMargin type="default" size="xs" title="Default"/>
                        </p>
                    </WidgetContent>
                </Widget>            
                <Widget column={4} size="lg" title={"Outline and Block Buttons"} tools>
                    <WidgetContent>     
                        <p>
                            Create block level buttons or outline buttons, by adding <code>.btn-block</code>   or <code>.btn-outline</code>.
                        </p>

                        <h3 className="font-bold">Outline buttons</h3>
                        <p>
                            <Button floatMargin outline type="default" title="Default"/>
                            <Button floatMargin outline type="primary" title="Primary"/>
                            <Button floatMargin outline type="success" title="Success"/>
                            <Button floatMargin outline type="info" title="Info"/>
                            <Button floatMargin outline type="warning" title="Warning"/>
                            <Button floatMargin outline type="danger" title="Danger"/>
                            <Button floatMargin outline type="link" title="Link"/>
                        </p>
                        <h3 className="font-bold">Block buttons</h3>
                        <p>
                            <button type="button" className="btn btn-block btn-outline btn-primary">Primary</button>
                        </p>
                    </WidgetContent>
                </Widget>                     
                <Widget column={12} size="lg" title={"3D Buttons"} tools>
                    <WidgetContent>                
                        <p>
                            To add three diminsion to buttons You can add <code>.dim</code> class to button.
                        </p>
                        <h3 className="font-bold">Three diminsion button</h3>                        
                            <Button type="primary" largeDim icon="money"/>
                            <Button type="warning" largeDim icon="warning"/>
                            <Button type="danger" largeDim icon="heart"/>
                            <Button type="primary" largeDim icon="dollar"/>
                            <Button type="info" outline largeDim icon="ruble"/>  

                            <Button type="primary" dim icon="money"/>
                            <Button type="warning" dim icon="warning"/>
                            <Button type="primary" dim icon="check"/>
                            <Button type="success" dim icon="upload"/>
                            <Button type="info" dim icon="paste"/>
                            <Button type="warning" dim icon="warning"/>
                            <Button type="default" dim icon="star"/>
                            <Button type="danger" dim icon="heart"/>

                            <Button outline type="primary" dim icon="money"/>
                            <Button outline type="warning" dim icon="warning"/>
                            <Button outline type="primary" dim icon="check"/>
                            <Button outline type="success" dim icon="upload"/>
                            <Button outline type="info" dim icon="paste"/>
                            <Button outline type="warning" dim icon="warning"/>
                            <Button outline type="default" dim icon="star"/>
                            <Button outline type="danger" dim icon="heart"/>
                    </WidgetContent>
                </Widget>
                <div className="col-lg-12">                    
                    <Row>
                        <Widget column="6" size="lg" title={"Button dropdowns"} tools>
                            <WidgetContent>
                                <p>
                                    Droppdowns buttons are avalible with any color and any size.
                                </p>

                                <h3 className="font-bold">Dropdowns</h3>
                                <DropdownButton title={"Action "} type="primary">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#" className="font-bold">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li className="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                </DropdownButton>
                                <DropdownButton title={"Action "} type="warning">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#" className="font-bold">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li className="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                </DropdownButton>
                                <DropdownButton title={"Action "} type="dafault">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#" className="font-bold">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li className="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                </DropdownButton>
                                <br/>
                                <DropdownButton title={"Action "} type="primary" size="sm">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#" className="font-bold">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li className="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                </DropdownButton>
                                <DropdownButton title={"Action "} type="warning" size="sm">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#" className="font-bold">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li className="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                </DropdownButton>
                                <DropdownButton title={"Action "} type="default" size="sm">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#" className="font-bold">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li className="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                </DropdownButton>
                                <br/>                                
                                <DropdownButton title={"Action "} type="primary" size="xs">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#" className="font-bold">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li className="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                </DropdownButton>
                                <DropdownButton title={"Action "} type="warning" size="xs">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#" className="font-bold">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li className="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                </DropdownButton>
                                <DropdownButton title={"Action "} type="default" size="xs">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#" className="font-bold">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li className="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                </DropdownButton>                           
                            </WidgetContent>
                        </Widget>
                        <Widget column="6" size="lg" title={"Grouped Buttons"} tools>
                            <WidgetContent>                    
                                <p>
                                    This is a group of buttons, ideal for sytuation where many actions are related to same element.
                                </p>

                                <h3 className="font-bold">Button Group</h3>
                                <ButtonGroup>                                
                                    <Button type="default" title="Left"/>
                                    <Button type="primary" title="Middle"/>
                                    <Button type="default" title="Right"/>
                                </ButtonGroup>
                                <br/>
                                <br/>
                                <ButtonGroup>  
                                    <Button type="default" icon="chevronLeft"/>
                                    <Button type="default" title="1"/>
                                    <Button type="default" active title="2"/>
                                    <Button type="default" title="3"/>
                                    <Button type="default" title="4"/>
                                    <Button type="default" icon="chevronRight"/>
                                </ButtonGroup>
                            </WidgetContent>
                        </Widget>
                    </Row>
                    <Widget column={12} size="lg" title={"Icon Buttons"} tools>
                        <WidgetContent>
                            <p>
                                To buttons with any color or any size you can add extra icon on the left or the right side.
                            </p>
                            <h3 className="font-bold">Commom Icon Buttons</h3>
                            <p>
                                <Button type="primary" icon="check">&nbsp;Submit</Button>
                                <Button type="success" icon="upload">&nbsp;&nbsp;<span className="bold">Upload</span></Button>
                                <Button type="info" icon="check">&nbsp;&nbsp;Edit</Button>
                                <Button type="warning" icon="check"><span className="bold">&nbsp;&nbsp;Warning</span></Button>
                                <Button type="default" icon="mapMarker">&nbsp;&nbsp;Map</Button>

                                <Button href="#" type="success" icon="facebook" title=" Sign in with Facebook"/>
                                <Button href="#" type="success" outline icon="facebook" title=" Sign in with Facebook"/>
                                
                                <Button href="#" type="default" icon="userMd"/>
                                <Button href="#" type="default" icon="group"/>
                                <Button href="#" type="default" icon="wrench"/>
                                <Button href="#" type="default" icon="exchange"/>
                                <Button href="#" type="default" icon="checkCircleO"/>
                                <Button href="#" type="default" icon="road"/>
                                <Button href="#" type="default" icon="ambulance"/>
                                <Button href="#" type="default" icon="star"/>
                            </p>
                            <h3 className="font-bold">Toggle buttons Variations</h3>
                            <p>Button groups can act as a radio or a switch or even a single toggle. Below are some examples click to see what happens</p>                            
                                <Button href="#" type="primary" outline icon="star" title="Single Toggle"/>
                                <Button href="#" type="primary" icon="star" title="Single Toggle"/>
                            <div data-toggle="buttons-checkbox" className="btn-group">
                                <button className="btn btn-primary active" type="button"><i className="fa fa-bold"></i> Bold</button>
                                <button className="btn btn-primary" type="button"><i className="fa fa-underline"></i> Underline</button>
                                <button className="btn btn-primary active" type="button"><i className="fa fa-italic"></i> Italic</button>
                            </div>
                        </WidgetContent>
                    </Widget>
                </div>
                <div className="col-lg-12">
                    <Row>                  
                        <Widget column={6} size="lg" title={"3D Buttons"} tools>
                            <WidgetContent> 
                                <p>
                                    For buttons you can add <code>.btn-circle</code> to rounded buttons and make it circle.
                                </p>
                                <h3 className="font-bold">Circle buttons</h3>
                                <br/>                                    
                                <Button floatMargin circle type="default" icon="check"/>
                                <Button floatMargin circle type="primary" icon="list"/>
                                <Button floatMargin circle type="success" icon="link"/>
                                <Button floatMargin circle type="info" icon="check"/>
                                <Button floatMargin circle type="warning" icon="times"/>
                                <Button floatMargin circle type="danger" icon="heart"/>
                                <Button floatMargin outline circle type="danger" icon="heart"/>
                                <br/>
                                <br/>                                    
                                <Button floatMargin circle type="default" size="lg" icon="check"/>
                                <Button floatMargin circle type="primary" size="lg"  icon="list"/>
                                <Button floatMargin circle type="success" size="lg"  icon="link"/>
                                <Button floatMargin circle type="info" size="lg"  icon="check"/>
                                <Button floatMargin circle type="warning" size="lg"  icon="times"/>
                                <Button floatMargin circle type="danger" size="lg"  icon="heart"/>
                                <Button floatMargin outline circle type="danger" size="lg"  icon="heart"/>
                            </WidgetContent>
                        </Widget>
                        <Widget column={6} size="lg" title={"Rounded Buttons"} tools>
                            <WidgetContent> 
                                <p>
                                    You can also add <code>.btn-rounded</code> class to round buttons.
                                </p>

                                <h3 className="font-bold">Button Group</h3>
                                <p>                                    
                                    <Button floatMargin outline rounded type="default" title="Default" href="#"/>
                                    <Button floatMargin outline rounded type="primary" title="Primary" href="#"/>
                                    <Button floatMargin outline rounded type="success" title="Success" href="#"/>
                                    <Button floatMargin rounded type="info" title="Info" href="#"/>
                                    <Button floatMargin rounded type="warning" title="Warning" href="#"/>
                                    <Button floatMargin rounded type="danger" title="Danger" href="#"/>
                                    <Button floatMargin rounded outline type="danger" title="Link" href="#"/>
                                    <br/>
                                    <br/>                                            
                                    <Button floatMargin rounded block type="primary" title="Block rounded with icon button" icon="infoCircle" href="#"/>
                                </p>
                            </WidgetContent>
                        </Widget>
                    </Row>
                </div>
                </Page>
            );
    }
}

export default Buttons;

