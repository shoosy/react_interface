import React from 'react'
import { IndexRoute, Route, Link, IndexRedirect, Redirect } from 'react-router'

/* containers */
import AppContainer from './containers/App'
import Packages from './containers/Packages'
import Pkg from './containers/Pkg'
import Profile from './containers/Profile'
import Buttons from './containers/Elements/Buttons'
import BadgeLabelProgress from './containers/Elements/BadgeLabelProgress'
import Helpers from './containers/Elements/Helpers'
import Notifications from './containers/Elements/Notifications'
import Tabs from './containers/Elements/Tabs'


const routes = (
  <Route path='/' component={AppContainer}>
    <IndexRedirect to='/packages/react' />
    <Route path='/packages/:keyword' component={Packages} />
    <Route path='/pkg/:name' component={Pkg} />
    <Route path='/profile' component={Profile} />
    <Route path='/elemets/buttons' component={Buttons} />
    <Route path='/elemets/tags' component={BadgeLabelProgress} />
    <Route path='/elemets/helpers' component={Helpers} />
    <Route path='/elemets/notifications' component={Notifications} />
    <Route path='/elemets/tabs' component={Tabs} />
  </Route>
)

export default routes
