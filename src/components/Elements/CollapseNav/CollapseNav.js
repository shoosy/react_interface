import React, {PropTypes} from 'react';
import { connect } from 'react-redux';
import {Nav, Navbar, NavItem, NavDropdown, MenuItem} from 'react-bootstrap';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CollapseNav.css';
import Icon from '../../Icon';

class CollapseNav extends React.Component {
  constructor(props) {
      super(props);
        this.state = {
            counter: 1,
        }
   };

    static propTypes = {
        children: PropTypes.node.isRequired,
        name: PropTypes.string,
        arrow: PropTypes.bool.isRequired,
        counter: PropTypes.number.isRequired,
    };

    click(){
      console.log(this.state.counter);
      this.setState({counter: this.state.counter + 1 })
      this.forceUpdate();
    };

    render(){
        const classItem = this.props.arrow ? `${s.fa} ${s.arrowLeft}` : `${s.fa} ${s.arrowRight}`;
        return(
            <NavItem href="#" onClick={this.click.bind(this)}>
                <span className="nav-label">{this.state.counter}</span>
                <span className={classItem}></span>
                <Nav className="nav nav-second-level collapse">
                    {this.props.children}
                </Nav>
            </NavItem>
        );
    }
}

export default withStyles(s)(CollapseNav);