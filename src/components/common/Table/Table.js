import React from 'react';
import s from './Table.css';

class Table extends React.Component {

    render() {
        
        let {bordered} = this.props;

        bordered = bordered == undefined ? false : true;

        return (
                <table className={`${s.table} ${bordered ? s.bordered : ""}`}>
                    {this.props.children}
                </table>
            );
    }
}

export default Table;

