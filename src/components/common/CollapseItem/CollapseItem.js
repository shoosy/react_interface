import React from 'react';
import { Collapse, Button } from 'react-bootstrap';
import s from './CollapseItem.css';
import Tag from '../Tag';
import Icon from '../../Icon';

class CollapseItem extends React.Component {

    constructor(props) {
      super(props);
        this.state = {
            arrow: `${s.fa} ${s.arrow}`,
            li: null,
            open: false
        }
    };

    collapse(){        
        const isOpen = !this.state.open;

        this.setState({
            open: isOpen,
            arrow: isOpen === true ? `${s.fa} ${s.arrowDown}`: `${s.fa} ${s.arrow}`,
            li: isOpen === true ? s.active : null
        });
    }
    render() {
        let {tag, tagType, tagContent} = this.props;

        if(tag !== "badge" && tag !== "label")
            tag = null;
        if(tagType !== "primary" && tagType !== "success" && tagType !== "info" && tagType !== "warning" && tagType !== "danger" && tagType !== "inverse")
            tagType = null;


        const level = s[`${this.props.level === undefined ? 'second' : this.props.level}Level`];
        const levelText = `${s.nav} ${level}`; 
        return (
                <li className={this.state.li}>
                    <a onClick={this.collapse.bind(this)}>
                        <Icon name={this.props.icon}/>
                        <span className="nav-label">{this.props.name}</span>
                        {this.props.notArrow ? null : <span className={this.state.arrow}></span>}                        
                        {tag === null ? null : tag === "badge" ? <Tag badge type={tagType} right title={tagContent}/> : <Tag label type={tagType} right title={tagContent}/>}     
                    </a>
                    <Collapse in={this.state.open}>
                    <ul className={levelText}>
                        {this.props.children}
                    </ul>
                    </Collapse>
                </li>
            );
    }
}

export default CollapseItem;

