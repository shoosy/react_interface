import React from 'react';
import s from './Tag.css';

class Dropdown extends React.Component {

    render() {
        
        let {badge, label, type, right, left} = this.props;

        if(type !== "primary" && type !== "success" && type !== "info" && type !== "warning" && type !== "danger" && type !== "inverse")
            type = null;

        if(badge !== undefined)
            badge = "badge";
        else
            badge = null;

        if(label !== undefined)
            label = "label";
        else
            label = null;

        if(right !== undefined)
            right = "right";
        else
            right = null;

        if(left !== undefined)
            left = "left";
        else
            left = null;

        const classname = 
        `${badge !== null ? `badge ${s[badge]} `: ""}` +
        `${label !== null ? `label ${s[label]} `: ""}` +
        `${type !== null ? `${s[type]} `: ""}` +
        `${right !== null ? `${s[right]} `: ""}` +
        `${left !== null ? `${s[left]} `: ""}`;
        return (
             <span className={classname}>{this.props.title}</span>
            );
    }
}

export default Dropdown;

