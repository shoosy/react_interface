import React from 'react';
import s from './Progress.css';

class Progress extends React.Component {

    render() {
        let { striped, active, mini, small } = this.props;

        if(striped !== undefined)
            striped = true;
        else striped = false;
        
        if(active !== undefined)
            active = true;
        else active = false;
        
        if(mini !== undefined)
            mini = true;
        else mini = false;
        
        if(small !== undefined)
            small = true;
        else small = false;

        return (             
                <div className={`progress ${s.progress} ${striped ? "progress-striped": ""} ${active ? "active": ""} ${mini ? s.mini: ""} ${small ? s.small: ""}`}>
                    {this.props.children}
                </div>
            );
    }
}

export default Progress;

