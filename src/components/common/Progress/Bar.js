import React from 'react';
import s from './Progress.css';

class Bar extends React.Component {

    render() {
        let {max, min, value, type, title, right, left} = this.props;
        let custom = {};

        if(type !== "success" && type !=="warning" && type !== "navyLight" && type !== "info" && type !== "danger")
            type = null;
        
        if(left !== undefined)
            left = "left";
        else left = null;
        
        if(right !== undefined)
            right = "right";
        else right = null;

        custom.width = `${value}%`;
        if(right != null)
            custom.float = right;
        if(left != null)
            custom.float = left;

        return (   
                <div style={custom} aria-valuemax={max} aria-valuemin={min} aria-valuenow={value} role="progressbar" className={`progress-bar ${s.bar} ${type === null ? "" : s[type]}`}>
                    <span className="sr-only">{title}</span>
                </div>
            );
    }
}

export default Bar;
