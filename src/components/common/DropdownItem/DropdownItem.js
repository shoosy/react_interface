import React from 'react';
import { NavItem } from 'react-bootstrap';
import s from '../DropdownMenu/DropdownMenu.css';

class DropdownItem extends React.Component {

    render() { 
        const isDivider = this.props.divider === undefined ? false : true;
        return (
                isDivider ? <li className={s.divider}></li> : <NavItem href={this.props.href}>{this.props.title}</NavItem>                
            );
    }
}

export default DropdownItem;