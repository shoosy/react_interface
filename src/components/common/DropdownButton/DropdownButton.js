import React from 'react';
import { ButtonGroup } from 'react-bootstrap';
import Button from '../Button';
import Dropdown from '../Dropdown';

class DropdownButton extends React.Component {
    constructor(props) {
      super(props);
        this.state = {
            open: false
        }
    };

    click(){
        this.setState({open: !this.state.open})
    }

    close(){
        this.setState({open: false})
    }

    render() {

        const props = {...this.props};
        delete props.children;
        return (
            <ButtonGroup>
                <Button dropdownToggle caret {...props} onClick={this.click.bind(this)}/>
                <Dropdown open={this.state}>{this.props.children}</Dropdown>
            </ButtonGroup>
            );
    }
}

export default DropdownButton;

