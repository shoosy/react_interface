import React, { Component } from 'react'
import ReactDom from 'react-dom'
import { Link } from 'react-router'
import newId from '../../utils/newId'


class Alert extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            id: newId()
        }
    }
    unmount(){
        let {children, type, dismissable} = this.props;
        ReactDom.unmountComponentAtNode(document.getElementById(this.state.id));
    }
    destroyRender(){
        let {children, type, dismissable} = this.props;

        if(type !== "info" && type !== "warning" && type !== "success" && type !== "danger")
            type = "success";

        let classname = `alert alert-${type}${dismissable ? " alert-dismissable" : ""}`
        return(                             
            <div className={classname}>
                <button aria-hidden="true" data-dismiss="alert" className="close" type="button" onClick={this.unmount.bind(this)}>×</button>
                {children}
            </div>   
        );
    }

    basicRender(){
        let {children, type, dismissable} = this.props;

        if(type !== "info" && type !== "warning" && type !== "success" && type !== "danger")
            type = "success";

        let classname = `alert alert-${type}${dismissable ? " alert-dismissable" : ""}`

        return(                                  
            <div className={classname}>
                {children}
            </div>
        );
    }
    componentDidMount(){
        
        let {children, type, dismissable} = this.props;

        if(type !== "info" && type !== "warning" && type !== "success" && type !== "danger")
            type = "success";

        let classname = `alert alert-${type}${dismissable ? " alert-dismissable" : ""}`

        dismissable = dismissable !== undefined ? true : false;

        ReactDom.render(                  
            dismissable ? this.destroyRender() : this.basicRender()
            ,document.getElementById(this.state.id))
    }
    render() {
        return(
            <div id={this.state.id}>
            </div>);
    }
}

export default Alert;
