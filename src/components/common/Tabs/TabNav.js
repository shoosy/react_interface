import React from 'react';
import s from './Tabs.css'

class TabNav extends React.Component {

        render() {
        let {children} = this.props;        

        return (     
            <ul className={`nav ${s.navTabs}`}>
                {children}
            </ul>    
            );
    }
}

export default TabNav;