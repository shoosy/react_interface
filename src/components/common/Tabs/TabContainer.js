import React from 'react';
import s from './Tabs.css'

class TabContainer extends React.Component {

        componentWillMount(){
            console.log("CONTAINER");
            console.log(this.props.children);
        }
        
        render() {
        let {lg, sm, xs, addClass, left, right} = this.props;

        let cName = 
        `col-${lg !== undefined ? `lg-${lg}` : sm !== undefined ? `sm-${sm}` : xs !== undefined ? `xs-${xs}` : 'lg-6'} ${addClass === undefined ? "" : addClass}`;
        let containerClass = 
        `${left !== undefined ? ` ${s.tabsLeft}`: ""}` +
        `${right !== undefined ? ` ${s.tabsRight}`: ""}`       

        return (     
            <div className={cName}>
            {(left !== undefined || right !== undefined) ? 
            <div className = {s.tabsContainer}>            
                <div className={containerClass}>
                    {this.props.children}
                </div> 
            </div> :            
            <div className={s.tabsContainer}>
                {this.props.children}
            </div> }   
            </div>      
            );
    }
}

export default TabContainer;