import React from 'react';
import s from './Tabs.css'

class TabContent extends React.Component {

        render() {
        let {children} = this.props;        

        return (     
            <div className={s.tabContent}>
                {children}
            </div>      
            );
    }
}

export default TabContent;