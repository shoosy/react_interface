import React from 'react';
import s from './Tabs.css'

class TabNavItem extends React.Component {

        render() {
        let {children, active, tableKey} = this.props;        
        return (             
                <li className={`${active === undefined ? "" : s.active}`}>
                    <a data-toggle="tab" href={`#${tableKey}`}>{children}</a>
                </li>   
            );
    }
}

export default TabNavItem;