import React from 'react';
import s from './Tabs.css'

class TabPanel extends React.Component {

        render() {
        let {tableKey, children, active} = this.props;        

        return (   
            <div id={tableKey === undefined ? "" : tableKey} className={`${s.tabPanel} ${active !== undefined ? s.active : ""}`}>
                <div className={s.panelBody}>
                    {children}
                </div>
            </div>      
            );
    }
}

export default TabPanel;