import React from 'react';
import { Collapse, Button } from 'react-bootstrap';
import ClickOutside from 'react-click-outside'
import s from './DropdownMenu.css';
import Dropdown from '../Dropdown';

class DropdownMenu extends React.Component {

    constructor(props) {
      super(props);
        this.state = {
            open: false
        }
    };

    click(){
        this.setState({open: !this.state.open})
    }

    close(){
        this.setState({open: false})
    }

    render() { 
        
        return (
            <div className={`dropdown profile-element ${this.state.open === false ? null : s.open}`}> <span>
                <img alt="image" className="img-circle" src="img/profile_small.jpg" />
                    </span>
                <a data-toggle="dropdown" className={s.dropdownToggle} href="#" onClick={this.click.bind(this)}>
                    {this.props.content}
                </a>
                <Dropdown open={this.state}>{this.props.children}</Dropdown>
            </div>
            );
    }
}
export default DropdownMenu;