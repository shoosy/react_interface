import React, { Component } from 'react'
import { Link } from 'react-router'
import { Button as Btn } from 'react-bootstrap'
import s from './Button.css'
import Icon from '../../Icon'

class Button extends React.Component {

  render() {
    let { icon, type, outline, circle, rounded, size, minSize, title, href, onClick, block, floatMargin, btn, active, dim, largeDim, caret, dropdownToggle, toggle, tooltip } = this.props;
    if(type !== "primary" && type !== "success" && type !== "info" && type !== "warning" && type !== "danger" && type !== "link" && type !== "default")
        type = null;

    if(circle !== undefined)
        circle = "circle";
    else
        circle = null;

    if(rounded !== undefined)
        rounded = "rounded";
    else
        rounded = null;

    if(minSize !== undefined)
        minSize = "minSize";
    else
        minSize = null;

    if(outline !== undefined)
        outline = "outline";
    else
        outline = null;

    if(block !== undefined)
        block = "block";
    else
        block = null;
        
    if(floatMargin !== undefined)
        floatMargin = "floatMargin";
    else
        floatMargin = null;

    if(floatMargin !== undefined)
        floatMargin = "floatMargin";
    else
        floatMargin = null;

    if(active !== undefined)
        active = "active";
    else
        active = null;

    if(dim !== undefined)
        dim = "dim";
    else
        dim = null;

    if(largeDim !== undefined)
        largeDim = "large";
    else
        largeDim = null;

    if(caret !== undefined)
        caret = "caret";
    else
        caret = null;

    if(dropdownToggle !== undefined)
        dropdownToggle = "dropdown-toggle";
    else
        dropdownToggle = null;

    if(size !== "lg" && size !== "sm" && size !== "xs")
        size = null;

    if(size !== "lg" && size !== "sm" && size !== "xs")
        size = null;

    if(tooltip === undefined)
        tooltip = null;

    const classname = 
    `btn ` +
    `${s.btn} ` +
    `${size != null ? `btn-${size} ` : ""}` +
    `${floatMargin != null ? `${s[floatMargin]} ` : ""}` +
    `${type != null ? `${s[type]} ` : ""}` +
    `${circle != null ? `${s[circle]} ` : ""}` + 
    `${dim != null ? `${s[dim]} ` : ""}` + 
    `${largeDim != null ? `${s.dim} ${s.large} ` : ""}` + 
    `${rounded != null ? `${s[rounded]} ` : ""}` +
    `${(size != null && size == "lg") ? `${s[size]} ` : ""}` + 
    `${minSize != null ? `${s[minSize]} ` : ""}` + 
    `${outline != null ? `${s[outline]} ` : ""}` +
    `${block != null ? `${s[block]} ` : ""}` +
    `${active != null ? `${s[active]} ` : ""}` +
    `${dropdownToggle != null ? `dropdown-toggle` : ""}`;
    
    return( 
        href !== undefined ? 
        <Link className={classname} to={href} onClick={onClick}>
            {icon === undefined ? null : <Icon name={icon}/>}
            {title}
            {this.props.children}
        </Link> :
            <Btn bsStyle={classname} onClick={onClick}>
                {icon === undefined ? null : <Icon name={icon}/>}
                {title}
                {caret === null ? null : <span className="caret"></span>}
                {this.props.children}
            </Btn> );
  }
}

export default Button;