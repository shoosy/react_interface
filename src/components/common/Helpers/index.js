import text from './text.css';
import background from './background.css';
import margin from './margin.css';
import image from './image.css';
import font from './font.css';
import border from './border.css';
import padding from './padding.css';
import borderRadius from './borderRadius.css';

exports.image = image;
exports.font = font;
exports.border = border;
exports.text = text;
exports.margin = margin;
exports.padding = padding;
exports.borderRadius = borderRadius;
exports.background = background;