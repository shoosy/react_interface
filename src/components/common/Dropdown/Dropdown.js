import React from 'react';
import s from './Dropdown.css';
import animate from '../animate.css';

class Dropdown extends React.Component {

    render() {
        let { open } = this.props; 
        let isOpen = false;
        if(open !== undefined && open.open === true)
            isOpen = true;
        else isOpen = false;
        return (
             <ul className={`${s.dropdownMenu} ${isOpen === true ? s["open"] : ""} ${animate.animated} ${animate.fadeInRight} ${s.mTxs}`}>
                {this.props.children}
             </ul>
            );
    }
}

export default Dropdown;

