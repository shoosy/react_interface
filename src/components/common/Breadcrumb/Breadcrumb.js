import React, { Component } from 'react'
import { Link } from 'react-router'
import s from './Breadcrumb.css'

class Breadcrumb extends React.Component {

  render() {
    const {title} = this.props;
    return(         

        <div className={`row ${s.wrapper}`}>
            <div className="col-lg-10">
                <h2>{title}</h2>
                <ol className={s.breadcrumb}>
                    {this.props.children}
                </ol>
            </div>
        </div>
        );
  }
}

export default Breadcrumb;