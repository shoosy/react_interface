import React, { Component } from 'react'
import NavLink from '../NavLink';
import s from './Breadcrumb.css'

class BreadcrumbLink extends React.Component {

    renderActive(title){
        return(
            <li className={s.active}>
                <strong>{title}</strong>
            </li>
        );
    }
     renderNotActive(title, to){
        return(
            <NavLink href={to}>{title}</NavLink>
        );
    }

  render() {
    let {title, to, active} = this.props;

    if(active !== undefined)
        active = true;
    else active = false;

    if(to === undefined)
        to = "#";

    if(active === true)
        return this.renderActive(title);
    else
        return this.renderNotActive(title, to);
  }
}

export default BreadcrumbLink;