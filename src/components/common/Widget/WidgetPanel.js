import React, { Component } from 'react'
import { Link } from 'react-router'
import s from './Widget.css'

class WidgetPanel extends React.Component {


  render() {
    let {children} = this.props;
        
    return(              
        <div className={`${s.ibox} float-e-margins`}>
            {children}
        </div>);
  }
}

export default WidgetPanel;