import React, { Component } from 'react'
import { Link } from 'react-router'
import s from './Widget.css'

class WidgetContent extends React.Component {

  render() {
    let { children, textCenter, id} = this.props;
    
    textCenter = textCenter !== undefined ? true : false;

    return(
        <div id={id} className={`${s.iboxContent} ${textCenter ? s.textCenter : ""}`}>
            { children }
        </div>);
  }
}

export default WidgetContent;