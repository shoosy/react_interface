import React, { Component } from 'react'
import { Link } from 'react-router'
import s from './Widget.css'
import Tools from './WidgetTools'
import Panel from './WidgetPanel'
import Title from './WidgetTitle'

class Widget extends React.Component {


  render() {
    let { column, title, children, tools, size } = this.props;
    
    if(size !== "lg" && size !== "md" && size !== "xs")
        size = null;

    
    return(
        column === undefined ?        
        <Panel>
            <Title title={title}>
                {tools === undefined ? null : <Tools />}    
            </Title>
            {children}
        </Panel>
        :
        <div className={`col-${size === null ? `md` : size}-${column}`}>
            <Panel>                
                <Title title={title}>
                    {tools === undefined ? null : <Tools />}    
                </Title>
                {children}
            </Panel>
        </div>);
  }
}

export default Widget;