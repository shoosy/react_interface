import Widget from './Widget';
import WidgetContent from './WidgetContent';
import WidgetPanel from './WidgetPanel';
import WidgetTitle from './WidgetTitle';

exports.Widget = Widget;
exports.WidgetContent = WidgetContent;
exports.WidgetPanel = WidgetPanel;
exports.WidgetTitle = WidgetTitle;