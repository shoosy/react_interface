import React, { Component } from 'react'
import { Link } from 'react-router'
import s from './Widget.css'
import Icon from '../../Icon'

class WidgetTools extends React.Component {

  render() {
    const { children} = this.props;
    return(            
            <div className={s.iboxTools}>
                <a className="collapse-link">
                    <Icon name="chevronUp"/>
                </a>
                <a className="dropdown-toggle" data-toggle="dropdown" href="#">
                    <Icon name="wrench"/>
                </a>
                <ul className="dropdown-menu dropdown-user">
                    <li><a href="#">Config option 1</a>
                    </li>
                    <li><a href="#">Config option 2</a>
                    </li>
                </ul>
                <a className="close-link">
                    <Icon name="times"/>
                </a>
            </div>
            );
  }
}

export default WidgetTools;