import React, { Component } from 'react'
import { Link } from 'react-router'
import s from './Widget.css'

class WidgetTitle extends React.Component {


  render() {
    let {children, title} = this.props;

    return(
        <div className={s.iboxTitle}>
            <h5>{title}</h5>
            {children}    
        </div>
        );
  }
}

export default WidgetTitle;