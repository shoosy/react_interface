import React, { Component } from 'react'
import { Link } from 'react-router'

class NavLink extends React.Component {

  render() {
    const { href, children, ...props } = this.props;
    return( 
    <li><Link to={href}>{children}</Link></li>);
  }
}

export default NavLink;