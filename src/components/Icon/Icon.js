import React, {PropTypes} from 'react';
import s from './fonts/css/font-awesome.css';
import CSSModules from 'react-css-modules'

@CSSModules(s)
class Icon extends React.Component {
    render(){
        return (
             <i className={s[this.props.name]}></i>
        );
    }
}

export default Icon;

