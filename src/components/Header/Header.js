import React, { PropTypes } from 'react';
import CSSModules from 'react-css-modules'
import {Nav, Navbar, NavItem, NavDropdown, MenuItem} from 'react-bootstrap';
import Icon from '../Icon/Icon';
import s from './Header.css';

@CSSModules(s)
class Header extends React.Component {

  render() {
    return (   
            
            <div className={`row ${s.borderBottom}`}>
                <nav className={`${s.navbar} ${s.navbarFixedTop}`} role="navigation">
                    <div className="navbar-header">
                        <a className={`navbar-minimalize  ${s.minimalizeStyl2} ${s.btn} ${s.btnPrimary}`} href="#">
                            <Icon name="bars"/> 
                        </a>
                        <form role="search" className={`${s.navbarFormCustom}`} action="search_results.html">
                            <div className={`${s.formGroup}`} >
                                <input type="text" placeholder="Search for something..." className={`form-control ${s.formControl}`}  name="top-search" id="top-search" />
                            </div>
                        </form>
                    </div>
                    <ul className={`nav navbar-top-links navbar-right ${s.navbarRight} ${s.navbarTopLinks} ${s.nav}`}>
                        <li>
                            <span className={`${s.mRsm} text-muted ${s.textMuted} ${s.welcomeMessage}`}>Welcome to INSPINIA+ Admin Theme.</span>
                        </li>
                        <li className="dropdown">
                            <a className="dropdown-toggle count-info" data-toggle="dropdown" href="#"> 
                                <Icon name="envelope"/> 
                                <span className="label label-warning">16</span>
                            </a>
                            <ul className="dropdown-menu dropdown-messages">
                                <li>
                                    <div className="dropdown-messages-box">
                                        <a href="profile.html" className="pull-left">
                                            <img alt="image" className="img-circle" src="img/a7.jpg" />
                                        </a>
                                        <div className="media-body">
                                            <small className="pull-right">46h ago</small>
                                            <strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br/>
                                            <small className="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
                                        </div>
                                    </div>
                                </li>
                                <li className="divider"></li>
                                <li>
                                    <div className="dropdown-messages-box">
                                        <a href="profile.html" className="pull-left">
                                            <img alt="image" className="img-circle" src="img/a4.jpg"/>
                                        </a>
                                        <div className="media-body ">
                                            <small className="pull-right text-navy">5h ago</small>
                                            <strong>Chris Johnatan Overtunk</strong> started following <strong>Monica Smith</strong>. <br/>
                                            <small className="text-muted">Yesterday 1:21 pm - 11.06.2014</small>
                                        </div>
                                    </div>
                                </li>
                                <li className="divider"></li>
                                <li>
                                    <div className="dropdown-messages-box">
                                        <a href="profile.html" className="pull-left">
                                            <img alt="image" className="img-circle" src="img/profile.jpg"/>
                                        </a>
                                        <div className="media-body">
                                            <small className="pull-right">23h ago</small>
                                            <strong>Monica Smith</strong> love <strong>Kim Smith</strong>. <br/>
                                            <small className="text-muted">2 days ago at 2:30 am - 11.06.2014</small>
                                        </div>
                                    </div>
                                </li>
                                <li className="divider"></li>
                                <li>
                                    <div className="text-center link-block">
                                        <a href="mailbox.html">                                            
                                            <Icon name="envelope"/><strong>Read All Messages</strong>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li className="dropdown">
                            <a className="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                <Icon name="bell"/>  <span className="label label-primary">8</span>
                            </a>
                            <ul className="dropdown-menu dropdown-alerts">
                                <li>
                                    <a href="mailbox.html">
                                        <div>                                            
                                                <Icon name="envelope"/>  You have 16 messages
                                            <span className="pull-right text-muted small">4 minutes ago</span>
                                        </div>
                                    </a>
                                </li>
                                <li className="divider"></li>
                                <li>
                                    <a href="profile.html">
                                        <div>
                                            
                                                <Icon name="twitter"/>3 New Followers
                                            <span className="pull-right text-muted small">12 minutes ago</span>
                                        </div>
                                    </a>
                                </li>
                                <li className="divider"></li>
                                <li>
                                    <a href="grid_options.html">
                                        <div>
                                            
                                            <Icon name="upload"/>  Server Rebooted
                                            <span className="pull-right text-muted small">4 minutes ago</span>
                                        </div>
                                    </a>
                                </li>
                                <li className="divider"></li>
                                <li>
                                    <div className="text-center link-block">
                                        <a href="notifications.html">
                                            <strong>See All Alerts</strong>
                                            
                                            <Icon name="angleRight"/> 
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>


                        <li>
                            <a href="login.html">                                
                                    <Icon name="signOut"/>  Log out
                            </a>
                        </li>
                    </ul>

                </nav>
            </div>      
    );
  }
}

export default Header; 