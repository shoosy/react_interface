/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import CSSModules from 'react-css-modules'
import React, { PropTypes } from 'react';
import s from './Layout.css';
import {background as b} from '../common/Helpers';
import Navigation from '../Navigation/Navigation';
import Footer from '../Footer/Footer';
import Header from '../Header/Header';
import {Breadcrumb, BreadcrumbLink} from '../common/Breadcrumb';

@CSSModules(s)
class Layout extends React.Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
  };

    constructor(props) {
      super(props);
        this.state = {
            counter: 1,
            mini: false,
        }
    };

    toggleNavigation(e) {
        this.setState({counter: this.state.counter + 1, mini: !this.state.mini});
    }

  render() {
    return (
      <div id="wrapper" className={s.wrapper}>
        <Navigation fixed mini={this.state.mini} counter={this.state.counter}/>
        <div id="page-wrapper" className={`${s.pageWrapper} ${b.gray}`}>    
            <Header/>            
            <Breadcrumb>
                <BreadcrumbLink to="/home" title="Home" />
                <BreadcrumbLink to="/elements" title="Elements" />
                <BreadcrumbLink active title="Helpers css classes" />
            </Breadcrumb>
            {this.props.children}
            <Footer/>
        </div>
      </div>
    );
  }
}

export default Layout;
