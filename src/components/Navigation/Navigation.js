import React, { Component } from 'react';
import { Link } from 'react-router'
import CSSModules from 'react-css-modules'
import { Dropdown, Collapse} from 'react-bootstrap';
import CollapseItem from '../common/CollapseItem';
import DropdownMenu from '../common/DropdownMenu';
import DropdownItem from '../common/DropdownItem';
import NavLink from '../common/NavLink';
import Icon from '../Icon';
import s from './Navigation.css';


@CSSModules(s)
class Navigation extends Component {

    render() {
        const isFixed = this.props.fixed === undefined ? false : true;
        return (     
        <nav className={`${s.navbarDefault} ${s.navbarStaticSide} ${isFixed ? s.fixedNav : ""}`} role="navigation">
            <div className={`sidebar-collapse ${isFixed ? s.scroll : ""}`}>
                <ul className={`${s.nav} metismenu`} id="side-menu">
                    <li className={s.navHeader}>
                        <DropdownMenu content={
                                <span className={s.clear}> 
                                    <span className={`${s.block} ${s.mTxs}`}> 
                                        <strong className={s.fontBold}>David Williams</strong>
                                    </span> 
                                    <span className={`${s.textMuted} text-xs ${s.block}`}>Art Director <b className={s.caret}></b>
                                    </span>                                
                                </span>
                            }>
                            <DropdownItem href="profile" title="Profile"/>
                            <DropdownItem href="profile" title="Contacts"/>
                            <DropdownItem href="profile" title="Mailbox"/>
                            <DropdownItem divider/>
                            <DropdownItem href="profile" title="Logout"/>
                        </DropdownMenu>
                        <div className={s.logoElement}>
                            IN+
                        </div>
                    </li>
                    <CollapseItem icon="thLarge" name={`${this.props.counter} + ${this.props.mini}`}>
                        <NavLink href="/">Dashboard v.1</NavLink>
                        <NavLink href='/profile'>Dashboard v.2</NavLink>
                        <NavLink href='/profile'>Dashboard v.3</NavLink>
                        <NavLink href='/profile'>Dashboard v.4</NavLink>
                        <NavLink href="dashboard_5.html">Dashboard v.5 </NavLink>
                    </CollapseItem>
                    <li>
                        <a to="login"><Icon name="diamond"/> <span className="nav-label">Layouts</span></a>
                    </li>
                    <CollapseItem icon="barChartO" name="Graphs">
                        <NavLink href="graph_flot.html">Flot Charts</NavLink>
                        <NavLink href="graph_morris.html">Morris.js Charts</NavLink>
                        <NavLink href="graph_rickshaw.html">Rickshaw Charts</NavLink>
                        <NavLink href="graph_chartjs.html">Chart.js</NavLink>
                        <NavLink href="graph_chartist.html">Chartist</NavLink>
                        <NavLink href="c3.html">c3 charts</NavLink>
                        <NavLink href="graph_peity.html">Peity Charts</NavLink>
                        <NavLink href="graph_sparkline.html">Sparkline Charts</NavLink>
                    </CollapseItem>
                    <CollapseItem icon="envelope" name="Mailbox" notArrow tag="label" tagType="warning" tagContent={"16/24"}>
                        <NavLink href="mailbox.html">Inbox</NavLink>
                        <NavLink href="mail_detail.html">Email view</NavLink>
                        <NavLink href="mail_compose.html">Compose email</NavLink>
                        <NavLink href="email_template.html">Email templates</NavLink>
                    </CollapseItem>
                    <li>
                        <a href="register"><Icon name="pieChart"/> <span className="nav-label">Metrics</span>  </a>
                    </li>
                    <li>
                        <a href="widgets.html"><Icon name="flask"/> <span className="nav-label">Widgets</span></a>
                    </li>
                    <CollapseItem icon="edit" name="Forms">
                        <NavLink href="form_basic.html">Basic form</NavLink>
                        <NavLink href="form_advanced.html">Advanced Plugins</NavLink>
                        <NavLink href="form_wizard.html">Wizard</NavLink>
                        <NavLink href="form_file_upload.html">File Upload</NavLink>
                        <NavLink href="form_editors.html">Text Editor</NavLink>
                        <NavLink href="form_autocomplete.html">Autocomplete</NavLink>
                        <NavLink href="form_markdown.html">Markdown</NavLink>
                    </CollapseItem>
                    <CollapseItem icon="desktop" name="App Views" notArrow content={<span className="pull-right label label-primary">SPECIAL</span>}>
                        <NavLink href="contacts.html">Contacts</NavLink>
                        <NavLink href="profile.html">Profile</NavLink>
                        <NavLink href="profile_2.html">Profile v.2</NavLink>
                        <NavLink href="contacts_2.html">Contacts v.2</NavLink>
                        <NavLink href="projects.html">Projects</NavLink>
                        <NavLink href="project_detail.html">Project detail</NavLink>
                        <NavLink href="activity_stream.html">Activity stream</NavLink>
                        <NavLink href="teams_board.html">Teams board</NavLink>
                        <NavLink href="social_feed.html">Social feed</NavLink>
                        <NavLink href="clients.html">Clients</NavLink>
                        <NavLink href="full_height.html">Outlook view</NavLink>
                        <NavLink href="vote_list.html">Vote list</NavLink>
                        <NavLink href="file_manager.html">File manager</NavLink>
                        <NavLink href="calendar.html">Calendar</NavLink>
                        <NavLink href="issue_tracker.html">Issue tracker</NavLink>
                        <NavLink href="blog.html">Blog</NavLink>
                        <NavLink href="article.html">Article</NavLink>
                        <NavLink href="faq.html">FAQ</NavLink>
                        <NavLink href="timeline.html">Timeline</NavLink>
                        <NavLink href="pin_board.html">Pin board</NavLink>
                    </CollapseItem>
                    <CollapseItem icon="filesO" name="Other Pages">
                        <NavLink href="search_results.html">Search results</NavLink>
                        <NavLink href="lockscreen.html">Lockscreen</NavLink>
                        <NavLink href="invoice.html">Invoice</NavLink>
                        <NavLink href="login.html">Login</NavLink>
                        <NavLink href="login_two_columns.html">Login v.2</NavLink>
                        <NavLink href="forgot_password.html">Forget password</NavLink>
                        <NavLink href="register.html">Register</NavLink>
                        <NavLink href="404.html">404 Page</NavLink>
                        <NavLink href="500.html">500 Page</NavLink>
                        <NavLink href="empty_page.html">Empty page</NavLink>
                    </CollapseItem>
                    <CollapseItem icon="globe" name="Miscellaneous" notArrow content={<span className="label label-info pull-right">NEW</span>}>
                        <NavLink href="toastr_notifications.html">Notification</NavLink>
                        <NavLink href="nestable_list.html">Nestable list</NavLink>
                        <NavLink href="agile_board.html">Agile board</NavLink>
                        <NavLink href="timeline_2.html">Timeline v.2</NavLink>
                        <NavLink href="diff.html">Diff</NavLink>
                        <NavLink href="pdf_viewer.html">PDF viewer</NavLink>
                        <NavLink href="i18support.html">i18 support</NavLink>
                        <NavLink href="sweetalert.html">Sweet alert</NavLink>
                        <NavLink href="idle_timer.html">Idle timer</NavLink>
                        <NavLink href="truncate.html">Truncate</NavLink>
                        <NavLink href="password_meter.html">Password meter</NavLink>
                        <NavLink href="spinners.html">Spinners</NavLink>
                        <NavLink href="spinners_usage.html">Spinners usage</NavLink>
                        <NavLink href="tinycon.html">Live favicon</NavLink>
                        <NavLink href="google_maps.html">Google maps</NavLink>
                        <NavLink href="datamaps.html">Datamaps</NavLink>
                        <NavLink href="social_buttons.html">Social buttons</NavLink>
                        <NavLink href="code_editor.html">Code editor</NavLink>
                        <NavLink href="modal_window.html">Modal window</NavLink>
                        <NavLink href="clipboard.html">Clipboard</NavLink>
                        <NavLink href="text_spinners.html">Text spinners</NavLink>
                        <NavLink href="forum_main.html">Forum view</NavLink>
                        <NavLink href="validation.html">Validation</NavLink>
                        <NavLink href="tree_view.html">Tree view</NavLink>
                        <NavLink href="loading_buttons.html">Loading buttons</NavLink>
                        <NavLink href="chat_view.html">Chat view</NavLink>
                        <NavLink href="masonry.html">Masonry</NavLink>
                        <NavLink href="tour.html">Tour</NavLink>
                    </CollapseItem>
                    <CollapseItem icon="flask" name="UI Elements">
                        <NavLink href="typography.html">Typography</NavLink>
                        <NavLink href="icons.html">Icons</NavLink>
                        <NavLink href="draggable_panels.html">Draggable Panels</NavLink>
                        <NavLink href="resizeable_panels.html">Resizeable Panels</NavLink>
                        <NavLink href="/elemets/buttons">Buttons</NavLink>
                        <NavLink href="video.html">Video</NavLink>
                        <NavLink href="tabs_panels.html">Panels</NavLink>
                        <NavLink href="/elemets/tabs">Tabs</NavLink>
                        <NavLink href="/elemets/notifications">Notifications & Tooltips</NavLink>
                        <NavLink href="/elemets/helpers">Helper css classes</NavLink>
                        <NavLink href="/elemets/tags">Badges, Labels, Progress</NavLink>
                    </CollapseItem>
                    <li>
                        <a href="grid_options.html"><Icon name="laptop"/> <span className="nav-label">Grid options</span></a>
                    </li>
                    <CollapseItem icon="table" name="Tables">
                        <NavLink href="table_basic.html">Static Tables</NavLink>
                        <NavLink href="table_data_tables.html">Data Tables</NavLink>
                        <NavLink href="table_foo_table.html">Foo Tables</NavLink>
                        <NavLink href="jq_grid.html">jqGrid</NavLink>
                    </CollapseItem>
                    <CollapseItem icon="shoppingCart" name="E-commerce">
                        <NavLink href="ecommerce_products_grid.html">Products grid</NavLink>
                        <NavLink href="ecommerce_product_list.html">Products list</NavLink>
                        <NavLink href="ecommerce_product.html">Product edit</NavLink>
                        <NavLink href="ecommerce_product_detail.html">Product detail</NavLink>
                        <NavLink href="ecommerce-cart.html">Cart</NavLink>
                        <NavLink href="ecommerce-orders.html">Orders</NavLink>
                        <NavLink href="ecommerce_payments.html">Credit Card form</NavLink>
                    </CollapseItem>
                    <CollapseItem icon="pictureO" name="Gallery">
                        <NavLink href="basic_gallery.html">Lightbox Gallery</NavLink>
                        <NavLink href="slick_carousel.html">Slick Carousel</NavLink>
                        <NavLink href="carousel.html">Bootstrap Carousel</NavLink>
                    </CollapseItem>
                    <CollapseItem icon="sitemap" name="Menu Levels">
                        <CollapseItem name="Third Level" level="third">
                            <NavLink href="#">Third Level Item</NavLink>
                            <NavLink href="#">Third Level Item</NavLink>
                            <NavLink href="#">Third Level Item</NavLink>
                        </CollapseItem>
                        <NavLink href="#">Second Level Item</NavLink>
                        <NavLink href="#">Second Level Item</NavLink>
                        <NavLink href="#">Second Level Item</NavLink>
                    </CollapseItem>
                    <li>
                        <a href="css_animation.html"><Icon name="magic"/> <span className="nav-label">CSS Animations </span><span className="label label-info pull-right">62</span></a>
                    </li>
                    <li className="landing_link">
                        <a target="_blank" href="landing.html"><Icon name="star"/> <span className="nav-label">Landing Page</span> <span className="label label-warning pull-right">NEW</span></a>
                    </li>
                    <li className="special_link">
                        <a href="package.html"><Icon name="database"/> <span className="nav-label">Package</span></a>
                    </li>
                </ul>
            </div>
        </nav>
        )
    }
}

export default Navigation;