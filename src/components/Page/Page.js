import React, { PropTypes } from 'react';
import s from './Page.css'
import animate from '../common/animate.css'

class Page extends React.Component {
  render() {

    let {animated, classAnimated} = this.props;

    if(animated === undefined)
        classAnimated = `${animate.animated} ${animate.fadeInRight}`;
    else 
        classAnimated = `${animate.animated} ${animate[animated]}`;

    return (        
            <div className={`${s.wrapperContent} ${classAnimated}`}> 
                {this.props.children}
            </div>
        );
    };
}
export default Page;