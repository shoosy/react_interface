import React, { PropTypes } from 'react';
import s from './Footer.css';

class Footer extends React.Component {
  render() {
    return (   
            <div className={s.footer}>
                <div className="pull-right">
                    10GB of <strong>250GB</strong> Free.
                </div>
                <div>
                    <strong>Copyright</strong> Example Company &copy; 2014-2017
                </div>
            </div>
    );
  }
}

export default Footer; 